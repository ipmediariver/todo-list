<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>
	<h1>Nuestro Proyecto de Prueba</h1>
	<p>Lorem, ipsum dolor sit amet consectetur adipisicing, elit. Quibusdam corporis cum, ducimus deleniti corrupti! Neque consectetur aliquam rem, error unde a perferendis aperiam eum, tempora voluptatem, nulla sapiente ut, ea.</p>

	@if (session('mensaje'))
	    <div class="alert alert-success">
	        {{ session('mensaje') }}
	    </div>
	@endif

	<form action="{{ route('tasks.store') }}" method="post">
		@csrf
		<input type="text" name="name">
		<input type="text" name="description">
		<button type="submit">Guardar</button>
	</form>

	@if(count($tasks))
		@foreach($tasks as $task)
			<p>{{ $task->name }}</p>
			<p><small>{{ $task->description }}</small></p>
			<a href="{{ route('tasks.edit', $task) }}">Editar</a>
			<form action="{{ route('tasks.destroy', $task) }}" method="POST" style="margin-top: 15px;">
				@csrf
				@method('DELETE')
				<button type="submit" style="color: red">Eliminar</button>
			</form>
			<hr>
		@endforeach
	@else
		No se han agregado tareas.
	@endif

</body>
</html>